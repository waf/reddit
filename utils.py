import json
import logging
import urllib.parse
import sys
from http.server import BaseHTTPRequestHandler, HTTPServer

import praw
import prawcore

import waflibs

# user agent string - <platform>:<app ID>:<version string> (by /u/<reddit username>)
PLATFORM = "cli"
APP_ID = "waf cli"
VERSION = "2021.12.28"
USERNAME = "fawwaf"
USER_AGENT = f"{PLATFORM}:{APP_ID}:v{VERSION} (by /u/{USERNAME})"
HOSTNAME = "localhost"
PORT = 8080

logger = logging.getLogger(waflibs.utils.PROGRAM_NAME)

logger.debug(f"user agent: {USER_AGENT}")

tokens = {}
body = """
authorization completed. you may close this window now and go back to the app/cli.
"""


class OauthServer(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        parsed = urllib.parse.urlsplit(self.path)
        query = urllib.parse.parse_qs(parsed.query)
        try:
            state = query["state"][0]
            if state not in tokens:
                state_dict = tokens[state] = {}
            state_dict["auth_token"] = query["code"][0]
        except KeyError as e:
            logger.debug(f"ignoring error - {e}")
            pass
        self.wfile.write(bytes(body, "utf-8"))


def start_webserver():
    web_server = HTTPServer((HOSTNAME, PORT), OauthServer)

    h = logging.StreamHandler()
    h.setLevel(logger.level)
    for name in ("praw", "prawcore"):
        l = logging.getLogger(name)
        l.setLevel(logger.level)
        l.addHandler(h)

    try:
        logger.debug(f"starting server at http://{HOSTNAME}:{PORT}")
        web_server.serve_forever()
    except KeyboardInterrupt:
        logger.debug("keyboard interrupt - stopping server")
        web_server.shutdown()
        web_server.server_close()


def get_credentials(filename, site_name):
    creds = None
    try:
        creds = waflibs.config.parse_json_file(filename)
        logger.debug(f"creds: {creds}")
    except (FileNotFoundError, json.decoder.JSONDecodeError) as e:
        logger.debug(f"file not found - {e}")

    reddit_args = {
        "user_agent": USER_AGENT,
        "redirect_uri": "http://localhost:8080",
    }

    if creds:
        reddit = praw.Reddit(
            site_name, refresh_token=creds["refresh_token"], **reddit_args
        )
    else:
        reddit = praw.Reddit(
            site_name,
            **reddit_args,
        )

        scopes = [
            "identity",
            "mysubreddits",
            "read",
            "subscribe",
        ]
        duration = "permanent"
        print(
            f"{site_name} account - go to this link in your browser: {reddit.auth.url(scopes, site_name, duration)}"
        )
        print()
        print("after visiting the link, hit ctrl-c to continue")

        start_webserver()

        logger.debug(f"tokens: {tokens}")
        auth_token = tokens["auth_token"]
        logger.debug(f"auth token: {auth_token}")

        refresh_token = reddit.auth.authorize(auth_token)
        logger.debug(f"refresh token: {refresh_token}")

        tokens = (
            {
                "auth_token": auth_token,
                "refresh_token": refresh_token,
            },
        )
        logger.debug(f"tokens: {tokens}")
        waflibs.utils.write_json_file(tokens, filename)

    return reddit
